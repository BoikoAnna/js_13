//Теоретичні питання

//Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
//setTimeout дозволяє викликати функцію один раз через певний інтервал часу
//setInterval дозволяє викликати функцію регулярно, повторюючи виклик через певний проміжок часу

//Що станеться, якщо в функцію setTimeout() передати нульову затримку? 
//Чи спрацює вона миттєво і чому?
//Цей метод буде викликаний відразу після того як завершиться виконання поточного коду,
//тобто реальна затримка ніколи не буде рівнятися 0. Задана затримка нуль лише означає, 
//що метод буде викликаний настільки швидко, настільки це можливо в календарі викликів.


//Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл 
//запуску вам вже не потрібен?
// функція та змінні на які вона посилаєтсья продовжуть займати пам'ять браузера.


//Завдання
//Реалізувати програму, яка циклічно показує різні картинки. 
//Завдання має бути виконане на чистому Javascript без використання бібліотек 
//типу jQuery або React.

//Технічні вимоги:
//У папці banners лежить HTML код та папка з картинками.
//При запуску програми на екрані має відображатись перша картинка.
//Через 3 секунди замість неї має бути показано друга картинка.
//Ще через 3 секунди – третя.
//Ще через 3 секунди – четверта.
//Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
//Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
//Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною
// та картинка, яка була там при натисканні кнопки.
//Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває 
//з тієї картинки, яка в даний момент показана на екрані.
//Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

//Необов'язкове завдання підвищеної складності
//При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує,
// скільки залишилося до показу наступної картинки.
//Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) 
//протягом 0.5 секунди.


// додаємо таймер та 2 кнопки показу (припинити, відновити)
document.body.insertAdjacentHTML('beforeend', `<div class="timer"></div>`);
document.body.insertAdjacentHTML('beforeend', `<button class="btn btn-stop">Припинити показ</button>`);
document.body.insertAdjacentHTML('beforeend', `<button class="btn btn-start" disabled>Відновити показ</button>`);


btnStop = document.querySelector('.btn-stop');
btnStart = document.querySelector('.btn-start');

btnStart.addEventListener('click', e => {
if (e.target.closest('.btn-start')) {
changeImg(count);
}
});

//вішаємо таймер, зупинка по кліку
btnStop.addEventListener('click', e => {
if (e.target.closest('.btn-stop')) {
console.log("stop");
timer.innerHTML = 2 + ' : ' + 999;
clearTimeout(imgTimerId);
clearInterval(timerId);
btnStop.setAttribute("disabled", '');
btnStart.removeAttribute("disabled")
}
});

//знаходимо зображення
const images = document.querySelectorAll('.image-to-show');
const timer = document.querySelector('.timer');
let count = 0;
let imgTimerId;
let timerId;


// прописуємо функцію зміни зображення 
function changeImg() {
if (count > images.length - 1) count = 0;
startTime() // додаємо зображення з класом active 
images[count].classList.add('active');
imgTimerId = setTimeout(() => {
images[count].classList.add('fade-out');
// видаляємо зображення з класом active  та fade-out (прозорість)
setTimeout(() => {
images[count].classList.remove('active', 'fade-out');
count++;

clearInterval(timerId);
// змінюємо картинку через 3 сек
changeImg()
}, 500);
}, 2500);
};
changeImg()

// функція таймеру
function startTime() {
console.log("tick");
let responseTime = new Date(Date.now() + (1000 * 3));
let countdown = new Date();
timerId = setInterval(function () {
countdown.setTime(responseTime - Date.now());
timer.innerHTML = (countdown.getUTCSeconds()) + ' : ' + countdown.getUTCMilliseconds();
}, 20);

btnStart.setAttribute("disabled", '');
btnStop.removeAttribute("disabled")
};
